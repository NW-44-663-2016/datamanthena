﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Microsoft.Data.Entity;

namespace DataManthenaSuprem.Models
{
    public class AppSeedData {
        public static void Initialize(IServiceProvider serviceProvider, string appPath)
        {
            string relPath = appPath + "//Models//SeedData//";
            var context = serviceProvider.GetService<AppDbContext>();
            var context1 = serviceProvider.GetService<AppDbContext>();
            context.Database.Migrate();
            if (context.Database == null)
            {
                throw new Exception("DB is null");
            }
            if (context.Locations.Any())
            {
                return;
            }


            if (context1.Database == null)
            {
                throw new Exception("DB is null");
            }
            context.Movies.RemoveRange(context.Movies);
            context.Locations.RemoveRange(context.Locations);
            context.SaveChanges();

            SeedLocationsFromCsv(relPath, context);
            SeedMoviesFromCsv(relPath, context);
        }
          /*  Location l1 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            Location l2 = new Location() { Latitude = 37.6991993, Longitude = -97.4843859, Place = "Wichita", State = "Kansas", Country = "USA" };
            Location l3 = new Location() { Latitude = 40.3494179, Longitude = -94.9238399, Place = "Maryville", State = "Missouri", Country = "USA" };
            Location l4 = new Location() { Latitude = 45.0993041, Longitude = -93.1007215, Place = "Whte Bear Lake", State = "Minnesota", Country = "USA" };
            Location l5 = new Location() { Latitude = 17.4126272, Longitude = 78.2676166, Place = "Hyderabad", Country = "India" };
            Location l6 = new Location() { Latitude = 25.9019385, Longitude = 84.6797775, Place = "Bihar", Country = "India" };
            context.Locations.AddRange(l1,l2,l3,l4,l5,l6 );
            context.SaveChanges();

            context1.Movies.AddRange(
                new Movie() { MovieTitle = "Need For Speed", Hero = "Dominic Cooper", Heroine = "Imogen Poots", Villan = "Aaron Paul", Director = "Scott Waugh", LocationID = l1.LocationID },
             new Movie() { MovieTitle = "Risen", Director = "Kevin Reynolds", Heroine = "María Botto", Hero = "Joseph Fiennes", Villan = "Tom Felton", LocationID = l2.LocationID },
            new Movie() { MovieTitle = "The Witch", Director = "Robert Eggers", Heroine = "Imogen Poots", Hero = "Dominic Cooper", Villan = "Aaron Paul", LocationID = l3.LocationID },
            new Movie() { MovieTitle = "Neerja", Director = "Ram Madhvani", Heroine = "Imogen Poots", Hero = "Dominic Cooper", Villan = "Aaron Paul", LocationID = l4.LocationID },
            new Movie() { MovieTitle = "The Revenant", Director = "Alejandro González Iñárritu", Heroine = "Imogen Poots", Hero = "Dominic Cooper", Villan = "Aaron Paul", LocationID = l5.LocationID },
            new Movie() { MovieTitle = "DeadPool", Director = "Tim Miller", Heroine = "Imogen Poots", Hero = "Dominic Cooper", Villan = "Aaron Paul", LocationID = l6.LocationID });
            
            context1.SaveChanges(); */

        private static void SeedMoviesFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "movie.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            Movie.ReadAllFromCSV(source);
            List<Movie> lst = Movie.ReadAllFromCSV(source);
            context.Movies.AddRange(lst.ToArray());
            context.SaveChanges();
        }

        private static void SeedLocationsFromCsv(string relPath, AppDbContext context)
        {
            string source = relPath + "location.csv";
            if (!File.Exists(source))
            {
                throw new Exception("Cannot find file " + source);
            }
            List<Location> lst = Location.ReadAllFromCSV(source);
            context.Locations.AddRange(lst.ToArray());
            context.SaveChanges();
        }







    }


}





