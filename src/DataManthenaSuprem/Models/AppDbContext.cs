﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Data.Entity;

namespace DataManthenaSuprem.Models
{
    public class AppDbContext : DbContext


    {
        public DbSet<Location> Locations { get; set; }
       // public DbSet<Mall> Malls { get; set; }
        public DbSet<Movie> Movies { get; set; }
    }
}
