﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.IO;

namespace DataManthenaSuprem.Models
{
    public class Movie
    {

        [ScaffoldColumn(false)]
        public int MovieID { get; set; }

        [Required]
        [Display (Name = " Title Name")]
        public string MovieTitle { get; set; }


        
             
     
        
        [Display(Name = " Hero Name")]
        public string Hero { get; set; }

        [Display(Name = " Heroine Name")]
        public string Heroine { get; set; }

        [Display(Name = " Villan Name")]
        public string Villan { get; set; }

        [Required]
        [Display(Name = " Director Name")]
        public string Director { get; set; }


        [ScaffoldColumn(false)]
        public int LocationID { get; set; }

        public virtual Location Location  { get; set; }

        public List<Location> filmSets { get; set; }

        public static List<Movie> ReadAllFromCSV(string filepath)
        {
            List<Movie> lst = File.ReadAllLines(filepath)
                                        .Skip(1)
                                        .Select(v => Movie.OneFromCsv(v))
                                        .ToList();
            return lst;
        }

        public static Movie OneFromCsv(string csvLine)
        {
            string[] values = csvLine.Split(',');
            Movie item = new Movie();

            int i = 0;
            item.MovieTitle = Convert.ToString(values[i++]);
            item.Hero = Convert.ToString(values[i++]);
            item.Heroine = Convert.ToString(values[i++]);
            item.Villan = Convert.ToString(values[i++]);
            item.Director = Convert.ToString(values[i++]);
            item.LocationID = Convert.ToInt32(values[i++]);

            return item;
        }

    



    }
}
