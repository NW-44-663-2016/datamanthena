using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataManthenaSuprem.Models;

namespace DataManthenaSuprem.Migrations
{
    [DbContext(typeof(AppDbContext))]
    [Migration("20160222041407_Initial")]
    partial class Initial
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataManthenaSuprem.Models.Location", b =>
                {
                    b.Property<int>("LocationID");

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<int?>("MovieMovieID");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataManthenaSuprem.Models.Movie", b =>
                {
                    b.Property<int>("MovieID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Director")
                        .IsRequired();

                    b.Property<string>("Hero");

                    b.Property<string>("Heroine");

                    b.Property<int>("LocationID");

                    b.Property<string>("MovieTitle")
                        .IsRequired();

                    b.Property<string>("Villan");

                    b.HasKey("MovieID");
                });

            modelBuilder.Entity("DataManthenaSuprem.Models.Location", b =>
                {
                    b.HasOne("DataManthenaSuprem.Models.Movie")
                        .WithMany()
                        .HasForeignKey("MovieMovieID");
                });

            modelBuilder.Entity("DataManthenaSuprem.Models.Movie", b =>
                {
                    b.HasOne("DataManthenaSuprem.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationID");
                });
        }
    }
}
