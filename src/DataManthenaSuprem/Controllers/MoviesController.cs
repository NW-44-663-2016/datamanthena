using System.Linq;
using Microsoft.AspNet.Mvc;
using Microsoft.AspNet.Mvc.Rendering;
using Microsoft.Data.Entity;
using DataManthenaSuprem.Models;

namespace DataManthenaSuprem.Controllers
{
    public class MoviesController : Controller
    {
        private AppDbContext _context;

        public MoviesController(AppDbContext context)
        {
            _context = context;    
        }

        // GET: Movies
        public IActionResult Index()
        {
            return View(_context.Movies.ToList());
        }

        // GET: Movies/Details/5
        public IActionResult Details(double? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Movie movie = _context.Movies.Single(m => m.MovieID == id);
            if (movie == null)
            {
                return HttpNotFound();
            }

            return View(movie);
        }

        // GET: Movies/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Movies/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Create(Movie movie)
        {
            if (ModelState.IsValid)
            {
                _context.Movies.Add(movie);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(movie);
        }

        // GET: Movies/Edit/5
        public IActionResult Edit(double? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Movie movie = _context.Movies.Single(m => m.MovieID == id);
            if (movie == null)
            {
                return HttpNotFound();
            }
            return View(movie);
        }

        // POST: Movies/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Edit(Movie movie)
        {
            if (ModelState.IsValid)
            {
                _context.Update(movie);
                _context.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(movie);
        }

        // GET: Movies/Delete/5
        [ActionName("Delete")]
        public IActionResult Delete(double? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Movie movie = _context.Movies.Single(m => m.MovieID == id);
            if (movie == null)
            {
                return HttpNotFound();
            }

            return View(movie);
        }

        // POST: Movies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public IActionResult DeleteConfirmed(double id)
        {
            Movie movie = _context.Movies.Single(m => m.MovieID == id);
            _context.Movies.Remove(movie);
            _context.SaveChanges();
            return RedirectToAction("Index");
        }
    }
}
